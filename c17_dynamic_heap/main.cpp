#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
    // 在栈上应用指针
    int number{50};
    int *pNumber = &number;
    // 不建议
    //  int *pNumber;
    // *pNumber = &number;
    cout << number << ": " << *pNumber << endl;
    cout << &number << ": " << pNumber << endl;

    // 堆上
    int *pNumber0{nullptr};
    pNumber0 = new int; // 这个地址就是固定的 我们就可以将这个地址的值设置为一个数
    *pNumber0 = 13;

    cout << *pNumber0 << endl;

    delete pNumber0;
    // delete pNumber0;
    pNumber0 = nullptr;

    // cout << *pNumber0 << endl;
    cout << "----- yz ------" << endl;
    return 0;
}