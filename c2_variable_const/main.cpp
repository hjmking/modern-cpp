#include <iostream>
#include <string>
using namespace std;

// const 与 #define
#define DPI 44 // 44 是什么 不需要关心

// 全局 一般不要用
int RandNum{0};
const double PI{3.1415926};

int main(int argc, char *argv[])
{
    cout << "Random Num : " << RandNum << endl;
    RandNum = 3;
    cout << "Random Num : " << RandNum << endl;
    // PI = 4.15;
    const int age = 45;
    const float oldWeight{88};

    // =、 {}
    float nowWeight{oldWeight + 30};
    cout << "age : " << age << endl;
    cout << "Weight : " << nowWeight << endl;

    int dpi_int{DPI};       // int
    string dpi_string{DPI}; // string

    cout << "dpi int " << dpi_int + 2 << endl;
    cout << "dpi string " << dpi_string << endl;
    cout << "dpi string "
         << "44 ascii ','" << endl;

    cout << "----- yz ------" << endl;
}