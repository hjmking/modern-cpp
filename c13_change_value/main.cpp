#include <iostream>
using namespace std;

// 返回
int addAge(int);
void addAgePoint(int &);

int main(int argc, char *argv[])
{
    int age{45};
    int newAge{addAge(45)};
    cout << newAge << endl;
    cout << "age: " << age << endl;
    cout << "&: " << &age << endl;
    addAgePoint(age);

    cout << age << endl;
    cout << "----- yz ------" << endl;
    return 0;
}

int addAge(int age)
{
    int result{age + 1};
    return result;
}

void addAgePoint(int &age)
{
    ++(age);
    cout << "age: " << age << " & :" << &age << endl;
}
